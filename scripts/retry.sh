#!/bin/sh

set -eu

retries=2
delay=3

usage() {
    cat <<EOF
$0  --  retry commands until success or retries are exceeded

Usage:

    $0 [options] [--] [command] [arguments...]

Options:
    --retries <N>   Number of retries after the inital failure  [default: N=$retries]
    --delay <S>     Seconds to wait before each retry attempt   [default: S=$delay]
    --help          Print this message and exit

EOF
}

while [ $# -gt 0 ]
do
    if [ "$1" = "--help" ]; then
        usage
        exit 0
    elif [ "$1" = "--retries" ]; then
        shift
        retries="$1"
        shift
    elif [ "$1" = "--delay" ]; then
        shift
        delay="$1"
        shift
    elif [ "$1" = "--" ]; then
        shift
        break
    else
        break
    fi
done

counter=0
code=0

until "$@"
do
    code="$?"
    counter=$(($counter + 1))
    [ "$counter" -gt "$retries" ] && break
    sleep "$delay"
    code=0
done

exit "$code"
